const ServerComponents = require('xc-core').Components;
let server = {};

const createHandler = require("azure-function-express").createHandler;


const getRawBody = require('raw-body');
const AliServer = require('@webserverless/fc-express').Server;
async function start() {

  try {

    console.time('Server started in');

    /* init server components */
    server = new ServerComponents(require('./app.components'));
    await server.init();
    server.router.start();

    /**************** START : print welcome banner ****************/
    const clear = require('clear');
    const Table = require('cli-table');

    // instantiate
    let table = new Table({
      head: ['Code Type', 'Code Path'],
      colWidths: [30, 60]
    });

    // table is an Array, so you can `push`, `unshift`, `splice` and friends
    table.push(
      ['Models', './server/components/databases/**/*.model.js'],
      ['GQL Schemas', './server/resolvers/**/*.schema.gen.js'],
      ['GQL Types', './server/resolvers/**/*.type.js'],
      ['GQL Resolvers', './server/resolvers/**/*.resolver.js'],
      ['Services', './server/services/**/*.service.js'],
      ['GQL Server Endpoint', `http://localhost:${server.$config.port}/graphql`],
    );

    clear();
    console.log(`\n\n\t\tGenerating GraphQL APIs at the speed of your thought..\n`.green);
    console.log(table.toString());
    console.log('\n');
    console.log(`\t\t\tTotal APIs Created : ${server.resolvers.apisCount()}\n\n\n\n`.green);

    console.timeEnd('Server started in');
    /**************** END : print welcome banner ****************/



  } catch (e) {
    console.log(e);
  }

}


const awsServerlessExpress = require('aws-serverless-express')
let appSingleton = null;

const init =  new Promise((resolve, reject) => {
  if (!appSingleton) {
    start().then(() => {
      if (server.$config.aws.lambda) {
        resolve(appSingleton = awsServerlessExpress.createServer(server.router.router))
      } else if (server.$config.azure.functionApp) {
        // resolve(appSingleton = createHandler(server.router.router))
        resolve(appSingleton = server.router.router);
      } else if (server.$config.gcp.cloudFunction) {
        resolve(appSingleton = server.router.router);
      }else if (server.$config.zeit.now) {
        resolve(appSingleton = server.router.router);
      }  else if (server.$config.alibaba.functionCompute) {
        resolve(appSingleton = new AliServer(server.router.router));
      } else {
        resolve(appSingleton = server.router.router);
      }
    }).catch((e) => reject(e))
  } else {
    resolve(appSingleton);
  }
})


module.exports =(...args) => {
  init.then( (appServer)=> {
    createHandler(appServer)(...args);
  })
}


module.exports.lambda = (event, context) => {
  init.then((appLambda) => {
    awsServerlessExpress.proxy(appLambda, event, context)
  })
}


module.exports.app = async function (...args) {
  const appServer = await init;
  return appServer(...args)
}



module.exports.zeit = function (req, res) {
  init.then(app => {
    app(req, res);
  })
}

module.exports.ali = async function (req, res, context) {
  req.body = await getRawBody(req);
  const server = await init;
  server.httpProxy(req, res, context)
}